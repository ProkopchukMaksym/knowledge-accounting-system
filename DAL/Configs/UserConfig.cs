﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace DAL.Configs
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasMany(e => e.CommentsByUser)
               .WithOne(e => e.UserSender)
               .HasForeignKey(e => e.UserSenderId);

            builder.HasMany(e => e.CommentsForUser)
               .WithOne(e => e.UserReceiver)
               .HasForeignKey(e => e.UserReceiverId);

            builder.HasMany(e => e.TagDescriptions)
               .WithOne(e => e.User)
               .HasForeignKey(e => e.UserId);

            var user = new User()
            {
                Id = -1,
                UserName = "admin@gmail.com",
                NormalizedUserName = "ADMIN@GMAIL.COM",
                Email = "admin@gmail.com",
                NormalizedEmail = "ADMIN@GMAIL.COM",
                FirstName = "Admin",
                LastName = "Admin",
                FullName = "Admin Admin",
                Country = "Ukraine",
                City = "Kiev",
                School = "Some school",
                University = "Some university",
                Speciality = "Computer science",
                Job = "Accounting system portal",
                LockoutEnabled = true,
                SecurityStamp = Guid.NewGuid().ToString()
        };
            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            user.PasswordHash = passwordHasher.HashPassword(user, "admin");

            builder.HasData(user);
        }
    }
}
