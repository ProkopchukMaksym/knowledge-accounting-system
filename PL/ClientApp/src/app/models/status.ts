export interface StatusModel {
  id: number;
  userStatus: string;
}
