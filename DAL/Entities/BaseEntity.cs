﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }

        public DateTime DateCreated { get; set; } = DateTime.Now;

        public DateTime LastModified { get; set; } = DateTime.Now;

        public bool IsDeleted { get; set; } = false;
    }
}
