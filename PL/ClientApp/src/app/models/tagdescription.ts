export interface TagDescriptionModel {
  id: number;
  tagId: number;
  tagName: string;
  description: string;
}
