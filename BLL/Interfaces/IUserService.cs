﻿using BLL.Models.In;
using BLL.Models.Out;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserPreviewModel>> GetPreviewUsersAsync();
        Task<IEnumerable<UserPreviewModel>> GetPreviewUsersByNameAsync(string userName);
        Task<UserProfileModel> GetUserProfileByIdAsync(int userId);
        Task AddUserAsync(UserRegisterModel registerModel);
        Task UpdateUserAsync(int userId, EditUserModel editUserModel);
        Task<UserTokenModel> LoginAsync(UserLoginModel loginModel);
        Task DeleteByIdAsync(int userId);
    }
}
