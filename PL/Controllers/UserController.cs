﻿using BLL.Interfaces;
using BLL.Models.In;
using BLL.Models.Out;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PL.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]s")]
    public class UserController : ControllerBase
    {
        private int UserId => int.Parse(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);

        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<ActionResult<UserTokenModel>> Login([FromBody] UserLoginModel userLoginModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userWithTokenModel = await _userService.LoginAsync(userLoginModel);

            return Ok(userWithTokenModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("register")]
        public async Task<ActionResult> Register([FromBody] UserRegisterModel userRegisterModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _userService.AddUserAsync(userRegisterModel);

            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<UserPreviewModel>> GetPreviewUsers()
        {
            var PreviewUserModels = await _userService.GetPreviewUsersAsync();
            return Ok(PreviewUserModels);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("via-username/{userName}")]
        public async Task<ActionResult<UserPreviewModel>> GetPreviewUsersByName(string userName)
        {
            var PreviewUserModels = await _userService.GetPreviewUsersByNameAsync(userName);
            return Ok(PreviewUserModels);
        }

        [HttpGet]
        [Authorize(Roles = "User,Admin")]
        [Route("{userId}")]
        public async Task<ActionResult<UserProfileModel>> GetUserProfileById(int userId)
        {
            var ProfileUserModel = await _userService.GetUserProfileByIdAsync(userId);
            return Ok(ProfileUserModel);
        }

        [HttpPatch]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> UpateUserById([FromBody] EditUserModel editUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _userService.UpdateUserAsync(UserId, editUserModel);

            return Ok();
        }

        [HttpPatch]
        [Authorize(Roles = "Admin")]
        [Route("via-admin/{userId}")]
        public async Task<ActionResult> UpateUserById([FromRoute] int userId, [FromBody] EditUserModel editUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _userService.UpdateUserAsync(userId, editUserModel);

            return Ok();
        }

        [HttpDelete]
        [Authorize(Roles = "Admin")]
        [Route("via-admin/{userId}")]
        public async Task<ActionResult> DeleteUser([FromRoute] int userId)
        {
            await _userService.DeleteByIdAsync(userId);
            return Ok();
        }
    }
}
