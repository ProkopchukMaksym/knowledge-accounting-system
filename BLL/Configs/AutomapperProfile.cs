﻿using AutoMapper;
using BLL.Models.In;
using BLL.Models.Out;
using DAL.Entities;
using System;
using System.Linq;

namespace BLL.Configs
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<User, UserTokenModel>()
                .ForMember(x => x.Role, opt => opt.MapFrom((src, dest, destMember, context) => (string)context.Items["role"]))
                .ForMember(x => x.Token, opt => opt.MapFrom((src, dest, destMember, context) => (string)context.Items["token"]))
                .ReverseMap();

            CreateMap<UserRegisterModel, User>()
                .ForMember(x => x.UserName, opt => opt.MapFrom(y => y.Email))
                .ForMember(x => x.FullName, opt => opt.MapFrom(y => y.FirstName + " " + y.LastName));

            CreateMap<User, UserPreviewModel>()
                .ForMember(x => x.Status, opt => opt.MapFrom(y => y.Status.UserStatus))
                .ForMember(x => x.Tags, opt => opt.MapFrom(y => y.TagDescriptions.Select(t => t.Tag.TagName)));

            CreateMap<User, UserProfileModel>()
                .ForMember(x => x.Status, opt => opt.MapFrom(y => y.Status.UserStatus));

            CreateMap<EditUserModel, User>()
                .ForMember(x => x.ImageLink, opt => opt.Condition(y => !String.IsNullOrEmpty(y.ImageLink)))
                .ForMember(x => x.FullName, opt => opt.MapFrom(y => y.FirstName + " " + y.LastName));

            CreateMap<AddTagDescriptionModel, TagDescription>();

            CreateMap<TagDescription, TagDescriptionModel>()
                .ForMember(x => x.TagName, opt => opt.MapFrom(y => y.Tag.TagName));

            CreateMap<Tag, TagModel>()
                .ReverseMap();

            CreateMap<Comment, CommentForUserModel>()
                .ForMember(x => x.ImageLinkUserSender, opt => opt.MapFrom(y => y.UserSender.ImageLink))
                .ForMember(x => x.FullNameUserSender, opt => opt.MapFrom(y => y.UserSender.FullName))
                .ForMember(x => x.Tags, opt => opt.MapFrom(y => y.CommentTags.Select(z => z.Tag).ToList()))
                .ForMember(x => x.DateCreated, opt => opt.MapFrom(y => y.DateCreated
                    .ToString("U")
                ));

            CreateMap<Comment, CommentByUserModel>()
                .ForMember(x => x.ImageLinkUserReceiver, opt => opt.MapFrom(y => y.UserReceiver.ImageLink))
                .ForMember(x => x.FullNameUserReceiver, opt => opt.MapFrom(y => y.UserReceiver.FullName))
                .ForMember(x => x.Tags, opt => opt.MapFrom(y => y.CommentTags.Select(z => z.Tag).ToList()))
                .ForMember(x => x.DateCreated, opt => opt.MapFrom(y => y.DateCreated
                    .ToString("U")
                ));

            CreateMap<AddCommentModel, Comment>()
                .ForMember(x => x.UserSenderId, opt => opt.MapFrom((src, dest, destMember, context) => (int)context.Items["userSenderId"]))
                .ForMember(x => x.CommentTags, opt => opt.MapFrom(y => y.TagIds));

            CreateMap<int, CommentTag>()
                .ForMember(x => x.TagId, opt => opt.MapFrom(y => y));

            CreateMap<Status, StatusModel>();
        }
    }
}
