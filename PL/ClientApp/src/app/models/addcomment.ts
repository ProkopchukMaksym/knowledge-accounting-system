export interface AddCommentModel {
  rating: number,
  text: string,
  userReceiverId: number,
  tagIds: number[]
}
