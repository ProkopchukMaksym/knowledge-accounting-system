export interface RegisterUserModel {
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  country: string,
  city: string,
  school: string,
  university: string,
  speciality: string,
  job: string
}
