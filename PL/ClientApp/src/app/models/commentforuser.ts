import { TagModel } from "./tag";

export interface CommentForUserModel {
  id: number;
  rating: number;
  text: string;
  userSenderId: number;
  imageLinkUserSender: string;
  fullNameUserSender: string;
  dateCreated: string;
  tags: TagModel[];
}
