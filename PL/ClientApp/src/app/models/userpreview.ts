export interface UserPreviewModel {
  id: number;
  imageLink: string;
  fullName: string;
  status: string;
  tags: string[];
  rating: number;
}
