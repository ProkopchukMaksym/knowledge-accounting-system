﻿using System;

namespace BLL.Configs
{
    public class JwtOptions
    {
        public string Secret { get; set; }
        public TimeSpan LifeTime { get; set; }
    }
}
