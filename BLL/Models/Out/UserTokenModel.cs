﻿namespace BLL.Models.Out
{
    public class UserTokenModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
    }
}
