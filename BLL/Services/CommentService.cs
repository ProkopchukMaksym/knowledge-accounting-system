﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.In;
using BLL.Models.Out;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CommentService : DisposableService, ICommentService
    {
        public CommentService(IUnitOfWork unit, IMapper mapper) : base(unit, mapper)
        {
        }
        public async Task<double> GetAverageRatingByUserIdAsync(int userId)
        {
            var comments = await _unit.CommentRepository.GetAllAsync();
            var commentsByUer = comments.Where(c => c.UserReceiverId == userId);

            if (!commentsByUer.Any())
            {
                return 0;
            }
            return commentsByUer.Average(c => c.Rating);
        }

        public async Task<IEnumerable<CommentByUserModel>> GetCommentsByUserIdAsync(int userId)
        {
            var comments = await _unit.CommentRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<CommentByUserModel>>(comments.Where(c => c.UserSenderId == userId).OrderBy(c => c.DateCreated));
        }

        public async Task<IEnumerable<CommentForUserModel>> GetCommentsForUserIdAsync(int userId)
        {
            var comments = await _unit.CommentRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<CommentForUserModel>>(comments.Where(c => c.UserReceiverId == userId).OrderBy(c => c.DateCreated));
        }

        public async Task AddCommentAsync(int userId, AddCommentModel commentModel) 
        {
            var userSender = await _unit.UserRepository.GetByIdAsync(userId);
            
            if (userSender == null)
            {
                throw new AccountingSystemException("User sender was not found");
            }

            var userReceiver = await _unit.UserRepository.GetByIdAsync(commentModel.UserReceiverId);

            if (userReceiver == null) 
            {
                throw new AccountingSystemException("User receiver was not found");
            }

            var comment = _mapper.Map<Comment>(commentModel, opt => { opt.Items["userSenderId"] = userId; });
            await _unit.CommentRepository.AddAsync(comment);
            
            await _unit.SaveAsync();
        }

        public async Task UpdateCommentAsync(int commentId, EditCommentModel editCommentModel)
        {
            var comment = await _unit.CommentRepository.GetByIdAsync(commentId);

            if (comment == null)
            {
                throw new AccountingSystemException("Comment was not found");
            }

            comment.Text = editCommentModel.Text;
            await _unit.SaveAsync();
        }

        public async Task DeleteByIdAsync(int commentId)
        {
            var comment = await _unit.CommentRepository.GetByIdAsync(commentId);

            if (comment == null)
            {
                throw new AccountingSystemException("Comment was not found");
            }

            var commentTags = comment.CommentTags;
            _unit.CommentTagRepository.RemoveRange(commentTags);
            _unit.CommentRepository.Remove(comment);
            await _unit.SaveAsync();
        }

        public async Task<bool> IsCommentBelongsUserIdAsync(int userId, int commentId) 
        {
            var comment = await _unit.CommentRepository.GetByIdAsync(commentId);

            if (comment == null)
            {
                throw new AccountingSystemException("Comment was not found");
            }
            return comment.UserSenderId == userId;
        }
    }
}
