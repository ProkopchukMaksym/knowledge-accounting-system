﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.In;
using BLL.Models.Out;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TagDescriptionService : DisposableService, ITagDescriptionService
    {
        public TagDescriptionService(IUnitOfWork unit, IMapper mapper) : base(unit, mapper)
        { }

        public async Task<IEnumerable<TagModel>> GetAllTagsAsync()
        {
            var tags = await _unit.TagRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<TagModel>>(tags);
        }

        public async Task<IEnumerable<TagDescriptionModel>> GetTagDescriptionsByUserIdAsync(int userId)
        {
            var tagDescriptions = await _unit.TagDescriptionRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<TagDescriptionModel>>(tagDescriptions.Where(td => td.UserId == userId));
        }
        
        public async Task AddTagDescriptionAsync(int userId, AddTagDescriptionModel tagDescriptionModel)
        {
            var user = await _unit.UserRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new AccountingSystemException("User was not found");
            }

            var tag = await _unit.TagRepository.GetByIdAsync(tagDescriptionModel.TagId);

            if (tag == null)
            {
                throw new AccountingSystemException("Tag was not found");
            }

            var tagDescriptions = await _unit.TagDescriptionRepository.GetAllAsync();
            var tagDescription = tagDescriptions.FirstOrDefault(td => 
                td.UserId == userId &&
                td.TagId == tagDescriptionModel.TagId);
            if (tagDescription == null)
            {
                var tagDescriptionToAdd = _mapper.Map<TagDescription>(tagDescriptionModel);
                tagDescriptionToAdd.UserId = userId;
                await _unit.TagDescriptionRepository.AddAsync(tagDescriptionToAdd);
            }
            else
            {
                _unit.TagDescriptionRepository.Update(_mapper.Map<AddTagDescriptionModel, TagDescription>(tagDescriptionModel, tagDescription));
            }
            await _unit.SaveAsync();
        }

        public async Task DeleteByIdAsync(int tagDescriptionId)
        {
            var tagDescription = await _unit.TagDescriptionRepository.GetByIdAsync(tagDescriptionId);

            if (tagDescription == null)
            {
                throw new AccountingSystemException("Tag description was not found");
            }
            _unit.TagDescriptionRepository.Remove(tagDescription);
            await _unit.SaveAsync();
        }

        public async Task<bool> IsTagDescriptionBelongsUserIdAsync(int userId, int tagDescriptionId) 
        {
            var tagDescription = await _unit.TagDescriptionRepository.GetByIdAsync(tagDescriptionId);

            if (tagDescription == null)
            {
                throw new AccountingSystemException("Tag description was not found");
            }
            return tagDescription.UserId == userId;
        }
    }
}
