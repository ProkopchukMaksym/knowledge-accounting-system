﻿using System.ComponentModel.DataAnnotations;

namespace BLL.Models.In
{
    public class UserLoginModel
    {
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Email format is incorrect")]
        [MinLength(5, ErrorMessage = "Email min length is 4 symbols")]
        [MaxLength(40, ErrorMessage = "Email max length is 20 symbols")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "Password is required")]
        [MinLength(4, ErrorMessage = "Password min length is 4 symbols")]
        [MaxLength(20, ErrorMessage = "Password max length is 20 symbols")]
        public string Password { get; set; }
    }
}
