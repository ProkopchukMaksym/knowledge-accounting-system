﻿using System.ComponentModel.DataAnnotations;

namespace BLL.Models.In
{
    public class AddTagDescriptionModel
    {
        [Required(ErrorMessage = "TagId is required")]
        public int TagId { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [MinLength(10, ErrorMessage = "Description min length is 10 symbols")]
        [MaxLength(1000, ErrorMessage = "Description max length is 1000 symbols")]
        public string Description { get; set; }
    }
}
