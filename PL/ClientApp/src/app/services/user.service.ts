import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { UserTokenModel } from '../models/usertoken';
import { UserPreviewModel } from '../models/userpreview';
import { UserProfileModel } from '../models/userprofile';
import { RegisterUserModel } from '../models/registeruser';
import { EditUserModel } from '../models/edituser';

@Injectable({ providedIn: 'root' })
export class UserService {
  private currentUserSubject: BehaviorSubject<UserTokenModel>;
  public currentUser: Observable<UserTokenModel>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<UserTokenModel>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): UserTokenModel {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string) {
    return this.http.post<UserTokenModel>(`${environment.apiUrl}/users/login`, { email, password })
      .pipe(map(userToken => {
        localStorage.setItem('currentUser', JSON.stringify(userToken));
        this.currentUserSubject.next(userToken);
        return userToken;
      }));
  }

  register(userRegisterModel: RegisterUserModel) {
    return this.http.post(`${environment.apiUrl}/users/register`, userRegisterModel);
  }

  logout(): void {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  getUserPreviews(): Observable<UserPreviewModel[]> {
    return this.http.get<UserPreviewModel[]>(`${environment.apiUrl}/users`);
  }

  getUserPreviewsByName(userName: string): Observable<UserPreviewModel[]> {
    return this.http.get<UserPreviewModel[]>(`${environment.apiUrl}/users/by-username/${userName}`);
  }

  getUserProfile(userId: number): Observable<UserProfileModel> {
    return this.http.get<UserProfileModel>(`${environment.apiUrl}/users/${userId}`);
  }

  editProfile(editUserModel: EditUserModel) {
    return this.http.patch(`${environment.apiUrl}/users`, editUserModel);
  }

  adminEditProfile(userId: number, editUserModel: EditUserModel) {
    return this.http.patch(`${environment.apiUrl}/users/via-admin/${userId}`, editUserModel);
  }

  adminDeleteUser(userId: number) {
    return this.http.delete(`${environment.apiUrl}/users/via-admin/${userId}`);
  }
}
