import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { StatusModel } from '../models/status';

@Injectable({ providedIn: 'root' })
export class StatusService {

  constructor(private http: HttpClient) {
  }

  editStatus(statusId: number) {
    return this.http.patch(`${environment.apiUrl}/statuses/${statusId}`, {});
  }

  adminEditStatus(userId: number, statusId: number) {
    return this.http.patch(`${environment.apiUrl}/statuses/via-admin/${userId}/${statusId}`, {});
  }

  getStatuses(): Observable<StatusModel[]> {
    return this.http.get<StatusModel[]>(`${environment.apiUrl}/statuses`);
  }
}
