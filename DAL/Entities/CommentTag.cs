﻿namespace DAL.Entities
{
    public class CommentTag : BaseEntity
    {
        public int TagId { get; set; }
        public virtual Tag Tag { get; set; }

        public int CommentId { get; set; }
        public virtual Comment Comment { get; set; }
    }
}
