﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AccountingSystemDbContext _context;

        private IUserRepository _userRepository;
        private IGenericRepository<Comment> _commentRepository;
        private IGenericRepository<CommentTag> _commentTagRepository;
        private IGenericRepository<Status> _statusRepository;
        private IGenericRepository<TagDescription> _tagDescriptionRepository;
        private IGenericRepository<Tag> _tagRepository;

        public UnitOfWork(AccountingSystemDbContext context, UserManager<User> userManagerRepository)
        {
            _context = context;
            UserManagerRepository = userManagerRepository;
        }

        public UserManager<User> UserManagerRepository { get; }

        public IGenericRepository<Comment> CommentRepository =>
            _commentRepository ??= new GenericRepository<Comment>(_context);

        public IGenericRepository<CommentTag> CommentTagRepository =>
            _commentTagRepository ??= new GenericRepository<CommentTag>(_context);

        public IGenericRepository<Status> StatusRepository =>
            _statusRepository ??= new GenericRepository<Status>(_context);

        public IGenericRepository<TagDescription> TagDescriptionRepository =>
            _tagDescriptionRepository ??= new GenericRepository<TagDescription>(_context);

        public IGenericRepository<Tag> TagRepository =>
            _tagRepository ??= new GenericRepository<Tag>(_context);

        public IUserRepository UserRepository =>
            _userRepository ??= new UserRepository(_context);

        public async Task<int> SaveAsync()
        {
            OnBeforeSaving();
            return await _context.SaveChangesAsync();
        }

        private void OnBeforeSaving()
        {
            var entries = _context.ChangeTracker.Entries();
            foreach (var entry in entries)
            {
                if (entry.Entity is BaseEntity trackable)
                {
                    var now = DateTime.Now;
                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            trackable.LastModified = now;
                            break;

                        case EntityState.Added:
                            trackable.DateCreated = now;
                            trackable.LastModified = now;
                            trackable.IsDeleted = false;
                            break;
                        case EntityState.Deleted:
                            entry.State = EntityState.Modified;
                            trackable.LastModified = now;
                            trackable.IsDeleted = true;
                            break;
                    }
                }
            }
        }

        private bool _isDisposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                _isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
