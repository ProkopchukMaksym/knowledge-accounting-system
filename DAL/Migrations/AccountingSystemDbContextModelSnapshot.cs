﻿// <auto-generated />
using System;
using DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DAL.Migrations
{
    [DbContext(typeof(AccountingSystemDbContext))]
    partial class AccountingSystemDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.14")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DAL.Entities.Comment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("datetime2");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime>("LastModified")
                        .HasColumnType("datetime2");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<string>("Text")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("UserReceiverId")
                        .HasColumnType("int");

                    b.Property<int>("UserSenderId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("UserReceiverId");

                    b.HasIndex("UserSenderId");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("DAL.Entities.CommentTag", b =>
                {
                    b.Property<int>("CommentId")
                        .HasColumnType("int");

                    b.Property<int>("TagId")
                        .HasColumnType("int");

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime>("LastModified")
                        .HasColumnType("datetime2");

                    b.HasKey("CommentId", "TagId");

                    b.HasIndex("TagId");

                    b.ToTable("CommentTags");
                });

            modelBuilder.Entity("DAL.Entities.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            ConcurrencyStamp = "4090e39e-a120-4bc2-bec3-2c7c210fd86e",
                            Name = "Admin",
                            NormalizedName = "ADMIN"
                        },
                        new
                        {
                            Id = 2,
                            ConcurrencyStamp = "10351a9e-5dff-4e14-8aa0-4de8dcf1babd",
                            Name = "User",
                            NormalizedName = "USER"
                        });
                });

            modelBuilder.Entity("DAL.Entities.Status", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("datetime2");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime>("LastModified")
                        .HasColumnType("datetime2");

                    b.Property<string>("UserStatus")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Statuses");

                    b.HasData(
                        new
                        {
                            Id = -1,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 515, DateTimeKind.Local).AddTicks(3289),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 518, DateTimeKind.Local).AddTicks(3426),
                            UserStatus = "Looking for new opportunities"
                        },
                        new
                        {
                            Id = -2,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 518, DateTimeKind.Local).AddTicks(5275),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 518, DateTimeKind.Local).AddTicks(5293),
                            UserStatus = "Studing"
                        },
                        new
                        {
                            Id = -3,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 518, DateTimeKind.Local).AddTicks(5317),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 518, DateTimeKind.Local).AddTicks(5321),
                            UserStatus = "Working"
                        },
                        new
                        {
                            Id = -4,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 518, DateTimeKind.Local).AddTicks(5325),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 518, DateTimeKind.Local).AddTicks(5328),
                            UserStatus = "On vacation"
                        });
                });

            modelBuilder.Entity("DAL.Entities.Tag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("datetime2");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime>("LastModified")
                        .HasColumnType("datetime2");

                    b.Property<string>("TagName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Tags");

                    b.HasData(
                        new
                        {
                            Id = -1,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3227),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3251),
                            TagName = "JavaScript"
                        },
                        new
                        {
                            Id = -2,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3910),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3924),
                            TagName = "Phyton"
                        },
                        new
                        {
                            Id = -3,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3944),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3947),
                            TagName = "C#"
                        },
                        new
                        {
                            Id = -4,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3951),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3954),
                            TagName = "DevOps"
                        },
                        new
                        {
                            Id = -5,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3958),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3962),
                            TagName = "Java"
                        },
                        new
                        {
                            Id = -6,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3969),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3972),
                            TagName = "C/C++"
                        },
                        new
                        {
                            Id = -7,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3976),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3980),
                            TagName = "Manager"
                        },
                        new
                        {
                            Id = -8,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3983),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3987),
                            TagName = "Lisp"
                        },
                        new
                        {
                            Id = -9,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3991),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(3994),
                            TagName = "Unity"
                        },
                        new
                        {
                            Id = -10,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(4000),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(4003),
                            TagName = "Teamlead"
                        },
                        new
                        {
                            Id = -11,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(4007),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(4010),
                            TagName = "Angular"
                        },
                        new
                        {
                            Id = -12,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(4014),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(4017),
                            TagName = "React"
                        },
                        new
                        {
                            Id = -13,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(4021),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(4024),
                            TagName = "Vue"
                        },
                        new
                        {
                            Id = -14,
                            DateCreated = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(4028),
                            IsDeleted = false,
                            LastModified = new DateTime(2021, 6, 6, 16, 7, 59, 519, DateTimeKind.Local).AddTicks(4032),
                            TagName = "TypeScript"
                        });
                });

            modelBuilder.Entity("DAL.Entities.TagDescription", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime>("LastModified")
                        .HasColumnType("datetime2");

                    b.Property<int>("TagId")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TagId");

                    b.HasIndex("UserId");

                    b.ToTable("TagDesctriptions");
                });

            modelBuilder.Entity("DAL.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("City")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Country")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FullName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ImageLink")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Job")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NormalizedEmail")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("School")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Speciality")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("StatusId")
                        .HasColumnType("int");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("University")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserName")
                        .HasColumnType("nvarchar(256)")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.HasIndex("StatusId");

                    b.ToTable("AspNetUsers");

                    b.HasData(
                        new
                        {
                            Id = -1,
                            AccessFailedCount = 0,
                            City = "Kiev",
                            ConcurrencyStamp = "5bd86d02-294e-42b5-b48d-dbd4efc9761a",
                            Country = "Ukraine",
                            Email = "admin@gmail.com",
                            EmailConfirmed = false,
                            FirstName = "Admin",
                            FullName = "Admin Admin",
                            ImageLink = "https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png",
                            Job = "Accounting system portal",
                            LastName = "Admin",
                            LockoutEnabled = true,
                            NormalizedEmail = "ADMIN@GMAIL.COM",
                            NormalizedUserName = "ADMIN@GMAIL.COM",
                            PasswordHash = "AQAAAAEAACcQAAAAEKutHNjSdINvXOyPqs2n316K+ZAjX+O0IjCApYJCxtHJv609i0a82ZugTvRNXKi34Q==",
                            PhoneNumberConfirmed = false,
                            School = "Some school",
                            SecurityStamp = "c545d5d1-d414-4e65-8802-199a726a81d7",
                            Speciality = "Computer science",
                            StatusId = -1,
                            TwoFactorEnabled = false,
                            University = "Some university",
                            UserName = "admin@gmail.com"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("RoleId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<int>", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.Property<int>("RoleId")
                        .HasColumnType("int");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");

                    b.HasData(
                        new
                        {
                            UserId = -1,
                            RoleId = 1
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("DAL.Entities.Comment", b =>
                {
                    b.HasOne("DAL.Entities.User", "UserReceiver")
                        .WithMany("CommentsForUser")
                        .HasForeignKey("UserReceiverId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.User", "UserSender")
                        .WithMany("CommentsByUser")
                        .HasForeignKey("UserSenderId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("DAL.Entities.CommentTag", b =>
                {
                    b.HasOne("DAL.Entities.Comment", "Comment")
                        .WithMany("CommentTags")
                        .HasForeignKey("CommentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.Tag", "Tag")
                        .WithMany("CommentTags")
                        .HasForeignKey("TagId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("DAL.Entities.TagDescription", b =>
                {
                    b.HasOne("DAL.Entities.Tag", "Tag")
                        .WithMany()
                        .HasForeignKey("TagId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.User", "User")
                        .WithMany("TagDescriptions")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("DAL.Entities.User", b =>
                {
                    b.HasOne("DAL.Entities.Status", "Status")
                        .WithMany("Users")
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.HasOne("DAL.Entities.Role", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<int>", b =>
                {
                    b.HasOne("DAL.Entities.Role", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.HasOne("DAL.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
