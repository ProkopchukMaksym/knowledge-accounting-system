﻿using BLL.Interfaces;
using BLL.Models.In;
using BLL.Models.Out;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PL.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]s")]
    public class CommentController : ControllerBase
    {
        private int UserId => int.Parse(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);

        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpPost]
        [Authorize(Roles = "User,Admin")]
        public async Task<ActionResult> AddComment([FromBody] AddCommentModel addCommentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (addCommentModel.UserReceiverId == UserId) 
            {
                return BadRequest("Leave a comment for your own page is forbidden");
            }

            await _commentService.AddCommentAsync(UserId, addCommentModel);

            return Ok();
        }

        [HttpGet]
        [Authorize(Roles = "User,Admin")]
        [Route("by-user/{userId}")]
        public async Task<ActionResult<IEnumerable<CommentByUserModel>>> GetCommentsByUser([FromRoute] int userId)
        {
            var commentByUserModels = await _commentService.GetCommentsByUserIdAsync(userId);

            return Ok(commentByUserModels);
        }

        [HttpGet]
        [Authorize(Roles = "User,Admin")]
        [Route("for-user/{userId}")]
        public async Task<ActionResult<IEnumerable<CommentForUserModel>>> GetCommentsForUser([FromRoute] int userId)
        {
            var commentForUserModels = await _commentService.GetCommentsForUserIdAsync(userId);

            return Ok(commentForUserModels);
        }

        [HttpPatch]
        [Authorize(Roles = "User")]
        [Route("{commentId}")]
        public async Task<ActionResult> UpateComment([FromRoute] int commentId, [FromBody] EditCommentModel editCommentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (!await _commentService.IsCommentBelongsUserIdAsync(UserId, commentId))
            {
                return BadRequest("Comment doesn't belong to user");
            }
            await _commentService.UpdateCommentAsync(commentId, editCommentModel);

            return Ok();
        }

        [HttpPatch]
        [Authorize(Roles = "Admin")]
        [Route("via-admin/{commentId}")]
        public async Task<ActionResult> AdminUpateComment([FromRoute] int commentId, [FromBody] EditCommentModel editCommentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _commentService.UpdateCommentAsync(commentId, editCommentModel);

            return Ok();
        }

        [HttpDelete]
        [Authorize(Roles = "User")]
        [Route("{commentId}")]
        public async Task<ActionResult> DeleteComment([FromRoute] int commentId)
        {
            if (!await _commentService.IsCommentBelongsUserIdAsync(UserId, commentId))
            {
                return BadRequest("Comment doesn't belong to user");
            }
            await _commentService.DeleteByIdAsync(commentId);
            return Ok();
        }

        [HttpDelete]
        [Authorize(Roles = "Admin")]
        [Route("via-admin/{commentId}")]
        public async Task<ActionResult> AdminDeleteComment([FromRoute] int commentId)
        {
            await _commentService.DeleteByIdAsync(commentId);
            return Ok();
        }
    }
}
