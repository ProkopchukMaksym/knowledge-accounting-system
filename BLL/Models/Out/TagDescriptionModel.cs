﻿namespace BLL.Models.Out
{
    public class TagDescriptionModel
    {
        public int Id { get; set; }
        public int TagId { get; set; } 
        public string TagName { get; set; }
        public string Description { get; set; }
    }
}
