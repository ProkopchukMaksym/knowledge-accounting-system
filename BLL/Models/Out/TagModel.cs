﻿namespace BLL.Models.Out
{
    public class TagModel
    {
        public int Id { get; set; }
        public string TagName { get; set; }
    }
}
