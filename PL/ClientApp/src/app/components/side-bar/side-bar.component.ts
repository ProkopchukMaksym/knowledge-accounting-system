import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserTokenModel } from '../../models/usertoken';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  currentUser: UserTokenModel;

  constructor(
    private router: Router,
    private userService: UserService
  ) {
  }

  ngOnInit() {
    this.userService.currentUser.subscribe(user => this.currentUser = user);
  }

  home(): void {
    this.router.navigate(['']);
  }

  profile(): void {
    if (this.currentUser) {
      this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
        this.router.navigate(['profile', this.currentUser.id]));
    }
    else {
      this.router.navigate(['login']);
    }
  }
}
