# Knowledge accounting system
This is a ASP.NET Core web-application and allows you find<br>
your future employee or employer and evaluate their work.

Anonymous can:
* register and login
* find and view users (without any personal data)

User can:
* what anonymous can
* edit personal data (only personal page)
* set status (only personal page)
* add descriptions about yourself and delete after (only personal page)
* guest other personal pages, leave comments and edit/delete after

Admin can:
* what user can
* edit personal data (for every user)
* set status (for every user)
* add descriptions about yourself and delete after (for every user)

## Backend part: 
* 3-layer architecture with low coupled DAL, BLL, PL (API)
* ORM Entity Framework Core
* Repository and Unit Of Work patterns
* Authentication Identity Core 
* ASP.NET Core REST API
* Automapper
* .NET Core DI

## Frontend part:
* Angular application
* Bootstrap 4.6
* Awesome icons
