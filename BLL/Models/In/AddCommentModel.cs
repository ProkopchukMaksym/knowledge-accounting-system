﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.Models.In
{
    public class AddCommentModel
    {
        [Required(ErrorMessage = "Rating is required")]
        [Range(1, 5, ErrorMessage = "Rating is in range 1 and 5")]
        public int Rating { get; set; }

        [Required(ErrorMessage = "Text is required")]
        [MinLength(10, ErrorMessage = "Text min length is 10 symbols")]
        [MaxLength(1000, ErrorMessage = "Text max length is 1000 symbols")]
        public string Text { get; set; }

        [Required(ErrorMessage = "UserReceiverId is required")]
        public int UserReceiverId { get; set; }

        [Required(ErrorMessage = "TagIds are required")]
        public ICollection<int> TagIds { get; set; }
    }
}