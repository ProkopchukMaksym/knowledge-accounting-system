export interface UserProfileModel
{
  id: number;
  imageLink: string;
  fullName: string;
  email: string;
  city: string;
  country: string;
  school: string;
  university: string;
  speciality: string;
  job: string;
  status: string;
  rating: number;
}
