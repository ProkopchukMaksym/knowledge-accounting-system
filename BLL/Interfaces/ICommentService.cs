﻿using BLL.Models.In;
using BLL.Models.Out;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICommentService
    {
        Task<double> GetAverageRatingByUserIdAsync(int userId);
        Task<IEnumerable<CommentByUserModel>> GetCommentsByUserIdAsync(int userId);
        Task<IEnumerable<CommentForUserModel>> GetCommentsForUserIdAsync(int userId);
        Task AddCommentAsync(int userId, AddCommentModel commentModel);
        Task UpdateCommentAsync(int commentId, EditCommentModel editCommentModel);
        Task DeleteByIdAsync(int commentId);
        Task<bool> IsCommentBelongsUserIdAsync(int userId, int commentId);
    }
}
