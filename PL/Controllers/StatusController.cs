﻿using BLL.Interfaces;
using BLL.Models.Out;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace PL.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]es")]
    public class StatusController : ControllerBase
    {
        private int UserId => int.Parse(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);

        private readonly IStatusService _statusService;

        public StatusController(IStatusService statusService)
        {
            _statusService = statusService;
        }

        [HttpPatch]
        [Authorize(Roles = "User")]
        [Route("{statusId}")]
        public async Task<ActionResult> SetStatus([FromRoute] int statusId)
        {
            await _statusService.SetStatusAsync(UserId, statusId);

            return Ok();
        }

        [HttpPatch]
        [Authorize(Roles = "Admin")]
        [Route("via-admin/{userId}/{statusId}")]
        public async Task<ActionResult> SetStatus([FromRoute] int userId, [FromRoute] int statusId)
        {
            await _statusService.SetStatusAsync(userId, statusId);

            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<StatusModel>>> GetAllStatuses()
        {
            var statusModels = await _statusService.GetAllStatusesAsync();

            return Ok(statusModels);
        }
    }
}
