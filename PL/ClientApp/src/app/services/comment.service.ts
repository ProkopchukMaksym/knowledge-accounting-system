import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { CommentForUserModel } from '../models/commentforuser';
import { CommentByUserModel } from '../models/commentbyuser';
import { AddCommentModel } from '../models/addcomment';

@Injectable({ providedIn: 'root' })
export class CommentService {

  constructor(private http: HttpClient) {
  }

  addComment(addCommentModel: AddCommentModel) {
    return this.http.post(`${environment.apiUrl}/comments`, addCommentModel);
  }

  getCommentsForUser(userId: number): Observable<CommentForUserModel[]> {
    return this.http.get<CommentForUserModel[]>(`${environment.apiUrl}/comments/for-user/${userId}`);
  }

  getCommentsByUser(userId: number): Observable<CommentByUserModel[]> {
    return this.http.get<CommentByUserModel[]>(`${environment.apiUrl}/comments/by-user/${userId}`);
  }

  editComment(commentId: number, text: string) {
    return this.http.patch(`${environment.apiUrl}/comments/${commentId}`,
      { text });
  }

  adminEditComment(commentId: number, text: string) {
    return this.http.patch(`${environment.apiUrl}/comments/via-admin/${commentId}`,
      { text });
  }

  deleteComment(commentId: number) {
    return this.http.delete(`${environment.apiUrl}/comments/${commentId}`);
  }

  adminDeleteComment(commentId: number) {
    return this.http.delete(`${environment.apiUrl}/comments/via-admin/${commentId}`);
  }
}
