export interface EditUserModel {
  firstName: string,
  lastName: string,
  imageLink: string,
  country: string,
  city: string,
  school: string,
  university: string,
  speciality: string,
  job: string
}
