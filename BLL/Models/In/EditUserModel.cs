﻿using System.ComponentModel.DataAnnotations;

namespace BLL.Models.In
{
    public class EditUserModel
    {
        [MaxLength(100, ErrorMessage = "ImageLink max length is 100 symbols")]
        public string ImageLink { get; set; }
        
        [Required(ErrorMessage = "FirstName is required")]
        [MinLength(2, ErrorMessage = "FirstName min length is 2 symbols")]
        [MaxLength(20, ErrorMessage = "FirstName max length is 20 symbols")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]
        [MinLength(2, ErrorMessage = "LastName min length is 2 symbols")]
        [MaxLength(20, ErrorMessage = "LastName max length is 20 symbols")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "City is required")]
        [MinLength(2, ErrorMessage = "City min length is 2 symbols")]
        [MaxLength(20, ErrorMessage = "City max length is 20 symbols")]
        public string City { get; set; }

        [Required(ErrorMessage = "Country is required")]
        [MinLength(2, ErrorMessage = "Country min length is 2 symbols")]
        [MaxLength(20, ErrorMessage = "Country max length is 20 symbols")]
        public string Country { get; set; }

        [MaxLength(40, ErrorMessage = "Max length is 40 symbols")]
        public string School { get; set; }

        [MaxLength(40, ErrorMessage = "University max length is 40 symbols")]
        public string University { get; set; }

        [MaxLength(40, ErrorMessage = "Speciality max length is 40 symbols")]
        public string Speciality { get; set; }

        [MaxLength(40, ErrorMessage = "Job max length is 40 symbols")]
        public string Job { get; set; }
    }
}
