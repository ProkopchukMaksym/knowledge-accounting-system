﻿using BLL.Interfaces;
using BLL.Models.In;
using BLL.Models.Out;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PL.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]s")]
    public class TagDescriptionController : ControllerBase
    {
        private int UserId => int.Parse(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);

        private readonly ITagDescriptionService _tagDescritionService;

        public TagDescriptionController(ITagDescriptionService tagDescritionService)
        {
            _tagDescritionService = tagDescritionService;
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> AddTagDescription([FromBody] AddTagDescriptionModel addTagDescriptionModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _tagDescritionService.AddTagDescriptionAsync(UserId, addTagDescriptionModel);

            return Ok();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("via-admin/{userId}")]
        public async Task<ActionResult> AddTagDescription([FromRoute] int userId, [FromBody] AddTagDescriptionModel addTagDescriptionModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _tagDescritionService.AddTagDescriptionAsync(userId, addTagDescriptionModel);

            return Ok();
        }

        [HttpGet]
        [Authorize(Roles = "User,Admin")]
        [Route("{userId}")]
        public async Task<ActionResult<IEnumerable<TagDescriptionModel>>> GetTagDescriptionsByUserId([FromRoute] int userId)
        {
            var tagDescriptionModels = await _tagDescritionService.GetTagDescriptionsByUserIdAsync(userId);

            return Ok(tagDescriptionModels);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("tags")]
        public async Task<ActionResult<IEnumerable<TagModel>>> GetAllTags()
        {
            var tagModels = await _tagDescritionService.GetAllTagsAsync();
            return Ok(tagModels);
        }

        [HttpDelete]
        [Authorize(Roles = "User")]
        [Route("{tagDescriptionId}")]
        public async Task<ActionResult> DeleteTagDescriptionTagId([FromRoute] int tagDescriptionId)
        {
            if (!await _tagDescritionService.IsTagDescriptionBelongsUserIdAsync(UserId, tagDescriptionId)) 
            {
                return BadRequest("Tag description doesn't belong to user");
            }
            await _tagDescritionService.DeleteByIdAsync(tagDescriptionId);
            return Ok();
        }

        [HttpDelete]
        [Authorize(Roles = "Admin")]
        [Route("via-admin/{tagDescriptionId}")]
        public async Task<ActionResult> AdminDeleteTagDescriptionTagId([FromRoute] int tagDescriptionId)
        {
            await _tagDescritionService.DeleteByIdAsync(tagDescriptionId);
            return Ok();
        }
    }
}
