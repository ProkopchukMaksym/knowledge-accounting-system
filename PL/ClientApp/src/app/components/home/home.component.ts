import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { UserPreviewModel } from '../../models/userpreview';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  findedUsers: UserPreviewModel[];

  constructor(
    private userService: UserService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.initFindUsers();
  }

  initFindUsers(): void {
    this.userService.getUserPreviews()
      .pipe(first())
      .subscribe(
        findedUsers => {
          this.findedUsers = findedUsers;
        });
  }

  findUsersByName(event: Event): void {
    let userName = (event.target as HTMLInputElement).value;
    if (userName) {
      this.userService.getUserPreviewsByName(userName)
        .pipe(first())
        .subscribe(
          findedUsers => {
            this.findedUsers = findedUsers;
          });
    }
    else {
      this.initFindUsers();
    }
  }

  redirectToUser(event): void {
    let idAttr: string = event.currentTarget.id.split("_")[1];
    this.router.navigate(['profile', idAttr]);
  }
}
