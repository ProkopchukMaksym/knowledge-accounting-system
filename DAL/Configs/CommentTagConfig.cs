﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Configs
{
    public class CommentTagConfig : IEntityTypeConfiguration<CommentTag>
    {
        public void Configure(EntityTypeBuilder<CommentTag> builder)
        {
            builder.HasKey(e => new { e.CommentId, e.TagId });

            builder.HasOne(x => x.Tag)
                .WithMany(x => x.CommentTags)
                .HasForeignKey(x => x.TagId);

            builder.HasOne(x => x.Comment)
               .WithMany(x => x.CommentTags)
               .HasForeignKey(x => x.CommentId);
        }
    }
}
