import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { AddCommentModel } from '../../models/addcomment';
import { CommentByUserModel } from '../../models/commentbyuser';
import { CommentForUserModel } from '../../models/commentforuser';
import { EditUserModel } from '../../models/edituser';
import { StatusModel } from '../../models/status';
import { TagModel } from '../../models/tag';
import { TagDescriptionModel } from '../../models/tagdescription';
import { UserProfileModel } from '../../models/userprofile';
import { UserTokenModel } from '../../models/usertoken';
import { CommentService } from '../../services/comment.service';
import { StatusService } from '../../services/status.service';
import { TagDescriptionService } from '../../services/tagdescription.sevice';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  editProfileForm: FormGroup;
  editStatusForm: FormGroup;
  addTagDescForm: FormGroup;
  addCommentForm: FormGroup;
  editCommentForm: FormGroup;

  loading: boolean = false;
  isOwner: boolean;
  isAdmin: boolean;
  userPageId: number;

  isCommentsForUser: boolean = true;
  
  commentIdToEditOrDelete: number;
  tagDescIdToDelete: number;

  currentUser: UserTokenModel;
  userProfile: UserProfileModel;
  statuses: StatusModel[];
  tagDescriptions: TagDescriptionModel[];
  tags: TagModel[];
  commentsForUser: CommentForUserModel[];
  commentsByUser: CommentByUserModel[];

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private tagDescriptionService: TagDescriptionService,
    private commentService: CommentService,
    private statusService: StatusService,
    private activeRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.userService.currentUser.subscribe(user => this.currentUser = user);
    this.activeRoute.params.subscribe(routeParams => {
      this.userPageId = parseInt(routeParams.id);
    });
    this.isOwner = this.currentUser.id == this.userPageId;
    this.isAdmin = this.currentUser.role == "Admin";

    this.getUserProfile();
    this.getTagDescriptions();
    this.getTags();
    this.getCommentsForUser();
    
    if (this.isOwner || this.isAdmin) {
      this.getCommentsByUser();
      this.getStatuses();
    }

    this.buildFormGroups();
  }

  buildFormGroups(): void {
    this.editProfileForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      imageLink: ['', [Validators.maxLength(100)]],
      country: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      city: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      school: ['', [Validators.maxLength(40)]],
      university: ['', [Validators.maxLength(40)]],
      speciality: ['', [Validators.maxLength(40)]],
      job: ['', [Validators.maxLength(40)]]
    });
    this.editStatusForm = this.formBuilder.group({
      statusId: ['', [Validators.required]]
    });
    this.addTagDescForm = this.formBuilder.group({
      tagId: ['', [Validators.required]],
      description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(1000)]]
    });
    this.addCommentForm = this.formBuilder.group({
      rating: ['3', [Validators.required, Validators.min(1), Validators.max(5)]],
      text: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(1000)]],
      tagId: ['', [Validators.required]]
    });
    this.editCommentForm = this.formBuilder.group({
      text: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(1000)]]
    });
  }

  get editProfileF() { return this.editProfileForm.controls; }
  get editStatusF() { return this.editStatusForm.controls; }
  get addTagDescF() { return this.addTagDescForm.controls; }
  get addCommentF() { return this.addCommentForm.controls; }
  get editCommentF() { return this.editCommentForm.controls; }

  onSetEditProfileForm(): void {
    this.editProfileForm.setValue({
      firstName: this.userProfile.fullName.split(" ")[0],
      lastName: this.userProfile.fullName.split(" ")[1],
      imageLink: this.userProfile.imageLink,
      country: this.userProfile.country,
      city: this.userProfile.city,
      school: this.userProfile.school,
      university: this.userProfile.university,
      speciality: this.userProfile.speciality,
      job: this.userProfile.job
    });
  }

  onSetEditCommentForm(commentId: number, text: string): void {
    this.editCommentForm.setValue({
      text: text
    });
    this.onSetCommentId(commentId);
  }

  getUserProfile(): void {
    this.userService.getUserProfile(this.userPageId)
      .pipe(first())
      .subscribe(
        userProfile => {
          this.userProfile = userProfile;
        });
  }

  getTagDescriptions(): void {
    this.tagDescriptionService.getTagDescriptions(this.userPageId)
      .pipe(first())
      .subscribe(
        tagDescriptions => {
          this.tagDescriptions = tagDescriptions;
        });
  }

  getTags(): void {
    this.tagDescriptionService.getTags()
      .pipe(first())
      .subscribe(
        tags => {
          this.tags = tags;
        });
  }

  getStatuses(): void {
    this.statusService.getStatuses()
      .pipe(first())
      .subscribe(
        statuses => {
          this.statuses = statuses;
        });
  }

  getCommentsForUser(): void {
    this.commentService.getCommentsForUser(this.userPageId)
      .pipe(first())
      .subscribe(
        commentsForUser => {
          this.commentsForUser = commentsForUser;
        });
  }

  getCommentsByUser(): void {
    this.commentService.getCommentsByUser(this.userPageId)
      .pipe(first())
      .subscribe(
        commentsByUser => {
          this.commentsByUser = commentsByUser;
        });
  }

  onEditProfile(): void {
    if (this.editProfileForm.invalid) {
      return;
    }
    this.loading = true;

    let editUserModel: EditUserModel = {
      firstName: this.editProfileF.firstName.value,
      lastName: this.editProfileF.lastName.value,
      imageLink: this.editProfileF.imageLink.value,
      country: this.editProfileF.country.value,
      city: this.editProfileF.city.value,
      school: this.editProfileF.school.value,
      university: this.editProfileF.university.value,
      speciality: this.editProfileF.speciality.value,
      job: this.editProfileF.job.value
    }

    let response: Observable<object>;

    if (this.isAdmin) {
      response = this.userService.adminEditProfile(this.userPageId, editUserModel);
    }
    else {
      response = this.userService.editProfile(editUserModel);
    }
    response.pipe(first())
      .subscribe(
        () => {
          this.getUserProfile();
          this.loading = false;
        });
  }

  onEditStatus(): void {
    if (this.editStatusForm.invalid) {
      return;
    }
    this.loading = true;

    let response: Observable<object>;

    if (this.isAdmin) {
      response = this.statusService.adminEditStatus(this.userPageId, this.editStatusF.statusId.value);
    }
    else {
      response = this.statusService.editStatus(this.editStatusF.statusId.value);
    }
    response.pipe(first())
      .subscribe(
        () => {
          this.getUserProfile();
          this.loading = false;
        });
  }

  onDeleteProfile(): void {
    this.userService.adminDeleteUser(
      this.userPageId
    ).pipe(first())
      .subscribe(
        () => {
          this.router.navigate(['']);
        });
  }

  onAddTagDescription(): void {
    if (this.addTagDescForm.invalid) {
      return;
    }
    this.loading = true;

    let response: Observable<object>;

    if (this.isAdmin) {
      response = this.tagDescriptionService.adminAddTagDescription(
        this.userPageId,
        parseInt(this.addTagDescF.tagId.value),
        this.addTagDescF.description.value
      );
    }
    else {
      response = this.tagDescriptionService.addTagDescription(
        parseInt(this.addTagDescF.tagId.value),
        this.addTagDescF.description.value
      );
    }
    response.pipe(first())
      .subscribe(
        () => {
          this.getTagDescriptions();
          this.loading = false;
        });
  }

  onSetTagDescId(tagDescId: number): void {
    this.tagDescIdToDelete = tagDescId;
  }

  onDeleteTagDesc(): void {
    this.loading = true;

    let response: Observable<object>;

    if (this.isAdmin) {
      response = this.tagDescriptionService.adminDeleteTagDescription(this.tagDescIdToDelete);
    }
    else {
      response = this.tagDescriptionService.deleteTagDescription(this.tagDescIdToDelete);
    }
    response.pipe(first())
      .subscribe(
        () => {
          this.getTagDescriptions();
          this.loading = false;
        });
  }

  onSetViewComments(isCommentsForUser: boolean): void {
    this.isCommentsForUser = isCommentsForUser;
  }

  onAddComment(): void {
    if (this.addCommentForm.invalid) {
      return;
    }
    this.loading = true;

    let addCommentModel: AddCommentModel = {
      rating: parseInt(this.addCommentF.rating.value),
      text: this.addCommentF.text.value,
      userReceiverId: this.userPageId,
      tagIds: [parseInt(this.addCommentF.tagId.value)]
    }

    this.commentService.addComment(addCommentModel)
      .pipe(first())
      .subscribe(
        () => {
          this.getUserProfile();
          this.getCommentsForUser();
          this.loading = false;
        });
  }

  onSetCommentId(commentId: number): void {
    this.commentIdToEditOrDelete = commentId;
  }

  onEditComment(): void {
    if (this.editCommentForm.invalid) {
      return;
    }
    this.loading = true;

    let response: Observable<object>;

    if (this.isAdmin) {
      response = this.commentService.adminEditComment(this.commentIdToEditOrDelete, this.editCommentF.text.value);
    }
    else {
      response = this.commentService.editComment(this.commentIdToEditOrDelete, this.editCommentF.text.value);
    }
    response.pipe(first())
      .subscribe(
        () => {
          this.getCommentsByUser();
          if (this.isAdmin) {
            this.getCommentsForUser();
            this.loading = false;
          }
        });
  }

  onDeleteComment() {
    this.loading = true;

    let response: Observable<object>;

    if (this.isAdmin) {
      response = this.commentService.adminDeleteComment(this.commentIdToEditOrDelete);
    }
    else {
      response = this.commentService.deleteComment(this.commentIdToEditOrDelete);
    }
    response.pipe(first())
      .subscribe(
        () => {
          this.getUserProfile();
          this.getCommentsByUser();
          if (this.isAdmin) {
            this.getCommentsForUser();
            this.loading = false;
          }
        });
  }

  redirectToUser(event): void {
    let idAttr: string = event.currentTarget.id.split("_")[1];
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this.router.navigate(['profile', idAttr]));
  }
}
