﻿using System.ComponentModel.DataAnnotations;

namespace BLL.Models.In
{
    public class EditCommentModel
    {
        [Required(ErrorMessage = "Text is required")]
        [MinLength(10, ErrorMessage = "Text min length is 10 symbols")]
        [MaxLength(1000, ErrorMessage = "Text max length is 1000 symbols")]
        public string Text { get; set; }
    }
}
