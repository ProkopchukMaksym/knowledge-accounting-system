import { TagModel } from "./tag";

export interface CommentByUserModel {
  id: number;
  rating: number;
  text: string;
  userReceiverId: number;
  imageLinkUserReceiver: string;
  fullNameUserReceiver: string;
  dateCreated: string;
  tags: TagModel[];
}
