import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TagDescriptionModel } from '../models/tagdescription';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { TagModel } from '../models/tag';

@Injectable({ providedIn: 'root' })
export class TagDescriptionService {
  
  constructor(private http: HttpClient) {
  }

  addTagDescription(tagId: number, description: string) {
    return this.http.post(`${environment.apiUrl}/tagdescriptions`, { tagId, description });
  }

  adminAddTagDescription(userId: number, tagId: number, description: string) {
    return this.http.post(`${environment.apiUrl}/tagdescriptions/via-admin/${userId}`, { tagId, description });
  }

  getTagDescriptions(userId: number): Observable<TagDescriptionModel[]> {
    return this.http.get<TagDescriptionModel[]>(`${environment.apiUrl}/tagdescriptions/${userId}`);
  }

  getTags(): Observable<TagModel[]> {
    return this.http.get<TagModel[]>(`${environment.apiUrl}/tagdescriptions/tags`);
  }

  deleteTagDescription(tagDescriptionId: number) {
    return this.http.delete(`${environment.apiUrl}/tagdescriptions/${tagDescriptionId}`);
  }

  adminDeleteTagDescription(tagDescriptionId: number) {
    return this.http.delete(`${environment.apiUrl}/tagdescriptions/via-admin/${tagDescriptionId}`);
  }
}

