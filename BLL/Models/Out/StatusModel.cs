﻿namespace BLL.Models.Out
{
    public class StatusModel
    {
        public int Id { get; set; }
        public string UserStatus { get; set; }
    }
}
