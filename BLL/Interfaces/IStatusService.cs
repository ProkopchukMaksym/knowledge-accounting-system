﻿using BLL.Models.Out;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IStatusService
    {
        Task<IEnumerable<StatusModel>> GetAllStatusesAsync();
        Task SetStatusAsync(int userId, int statusId);
    }
}
