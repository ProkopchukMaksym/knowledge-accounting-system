﻿using AutoMapper;
using DAL.Interfaces;
using System;

namespace BLL.Services
{
    public class DisposableService : IDisposable
    {
        protected readonly IUnitOfWork _unit;
        protected readonly IMapper _mapper;

        public DisposableService(IUnitOfWork unit, IMapper mapper)
        {
            _unit = unit;
            _mapper = mapper;
        }

        private bool _isDisposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _unit.Dispose();
                }
                _isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
