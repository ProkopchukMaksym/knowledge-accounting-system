﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace DAL.Configs
{
    public class TagConfig : IEntityTypeConfiguration<Tag>
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder.HasData(new List<Tag>
            {
                new Tag { Id = -1, TagName = "JavaScript" },
                new Tag { Id = -2, TagName = "Phyton" },
                new Tag { Id = -3, TagName = "C#" },
                new Tag { Id = -4, TagName = "DevOps" },
                new Tag { Id = -5, TagName = "Java" },
                new Tag { Id = -6, TagName = "C/C++" },
                new Tag { Id = -7, TagName = "Manager" },
                new Tag { Id = -8, TagName = "Lisp" },
                new Tag { Id = -9, TagName = "Unity" },
                new Tag { Id = -10, TagName = "Teamlead" },
                new Tag { Id = -11, TagName = "Angular" },
                new Tag { Id = -12, TagName = "React" },
                new Tag { Id = -13, TagName = "Vue" },
                new Tag { Id = -14, TagName = "TypeScript" }
            });
        }
    }
}
