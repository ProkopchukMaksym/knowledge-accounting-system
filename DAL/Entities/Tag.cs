﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class Tag : BaseEntity
    {
        public string TagName { get; set; }

        public virtual ICollection<CommentTag> CommentTags { get; set; } = new List<CommentTag>();
    }
}