﻿using BLL.Models.In;
using BLL.Models.Out;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ITagDescriptionService
    {
        Task<IEnumerable<TagModel>> GetAllTagsAsync();
        Task<IEnumerable<TagDescriptionModel>> GetTagDescriptionsByUserIdAsync(int userId);
        Task AddTagDescriptionAsync(int userId, AddTagDescriptionModel tagDescriptionModel);
        Task DeleteByIdAsync(int tagDescriptionId);
        Task<bool> IsTagDescriptionBelongsUserIdAsync(int userId, int tagDescriptionId);
    }
}
