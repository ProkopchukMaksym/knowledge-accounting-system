﻿using System.ComponentModel.DataAnnotations;

namespace BLL.Models.In
{
    public class UserRegisterModel
    {
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Email format is incorrect")]
        [MinLength(5, ErrorMessage = "Email min length is 4 symbols")]
        [MaxLength(40, ErrorMessage = "Email max length is 20 symbols")]
        public string Email { get; set; }

        [Required(ErrorMessage = "FirstName is required")]
        [MinLength(2, ErrorMessage = "FirstName min length is 2 symbols")]
        [MaxLength(20, ErrorMessage = "FirstName max length is 20 symbols")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]
        [MinLength(2, ErrorMessage = "LastName min length is 2 symbols")]
        [MaxLength(20, ErrorMessage = "LastName max length is 20 symbols")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "City is required")]
        [MinLength(2, ErrorMessage = "City min length is 2 symbols")]
        [MaxLength(20, ErrorMessage = "City max length is 20 symbols")]
        public string City { get; set; }

        [Required(ErrorMessage = "Country is required")]
        [MinLength(2, ErrorMessage = "Country min length is 2 symbols")]
        [MaxLength(20, ErrorMessage = "Country max length is 20 symbols")]
        public string Country { get; set; }

        [MaxLength(40, ErrorMessage = "School max length is 40 symbols")]
        public string School { get; set; }

        [MaxLength(40, ErrorMessage = "University max length is 40 symbols")]
        public string University { get; set; }

        [MaxLength(40, ErrorMessage = "Speciality max length is 40 symbols")]
        public string Speciality { get; set; }

        [MaxLength(40, ErrorMessage = "Job max length is 40 symbols")]
        public string Job { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [MinLength(4, ErrorMessage = "Password min length is 4 symbols")]
        [MaxLength(20, ErrorMessage = "Password max length is 20 symbols")]
        public string Password { get; set; }
    }
}
