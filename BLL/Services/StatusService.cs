﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.Out;
using BLL.Validation;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class StatusService : DisposableService, IStatusService
    {
        public StatusService(IUnitOfWork unit, IMapper mapper) : base(unit, mapper)
        {
        }

        public async Task<IEnumerable<StatusModel>> GetAllStatusesAsync()
        {
            var statuses = await _unit.StatusRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<StatusModel>>(statuses);
        }

        public async Task SetStatusAsync(int userId, int statusId) 
        {
            var user = await _unit.UserRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new AccountingSystemException("User was not found");
            }

            var status = await _unit.StatusRepository.GetByIdAsync(statusId);

            if (status == null)
            {
                throw new AccountingSystemException("Status was not found");
            }

            user.StatusId = statusId;
            await _unit.SaveAsync();
        }
    }
}
