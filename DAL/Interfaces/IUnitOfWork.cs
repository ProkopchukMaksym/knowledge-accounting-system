﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        UserManager<User> UserManagerRepository { get; }
        IUserRepository UserRepository { get; }
        IGenericRepository<Comment> CommentRepository { get; }
        IGenericRepository<CommentTag> CommentTagRepository { get; }
        IGenericRepository<Status> StatusRepository { get; }
        IGenericRepository<TagDescription> TagDescriptionRepository { get; }
        IGenericRepository<Tag> TagRepository { get; }
        Task<int> SaveAsync();
    }
}
