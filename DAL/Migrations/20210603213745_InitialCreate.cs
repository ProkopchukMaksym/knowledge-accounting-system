﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Statuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    UserStatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Statuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    TagName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ImageLink = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    School = table.Column<string>(nullable: true),
                    University = table.Column<string>(nullable: true),
                    Speciality = table.Column<string>(nullable: true),
                    Job = table.Column<string>(nullable: true),
                    StatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    UserReceiverId = table.Column<int>(nullable: false),
                    UserSenderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_AspNetUsers_UserReceiverId",
                        column: x => x.UserReceiverId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_AspNetUsers_UserSenderId",
                        column: x => x.UserSenderId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TagDesctriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    TagId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagDesctriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TagDesctriptions_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TagDesctriptions_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CommentTags",
                columns: table => new
                {
                    TagId = table.Column<int>(nullable: false),
                    CommentId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommentTags", x => new { x.CommentId, x.TagId });
                    table.ForeignKey(
                        name: "FK_CommentTags_Comments_CommentId",
                        column: x => x.CommentId,
                        principalTable: "Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CommentTags_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "83e2003d-c851-489b-b07e-969656d1ea26", "Admin", "ADMIN" },
                    { 2, "8a3abae5-820c-4cf9-9884-584005eca39c", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "Statuses",
                columns: new[] { "Id", "DateCreated", "IsDeleted", "LastModified", "UserStatus" },
                values: new object[,]
                {
                    { -1, new DateTime(2021, 6, 4, 0, 37, 44, 538, DateTimeKind.Local).AddTicks(7516), false, new DateTime(2021, 6, 4, 0, 37, 44, 541, DateTimeKind.Local).AddTicks(3488), "Looking for new opportunities" },
                    { -2, new DateTime(2021, 6, 4, 0, 37, 44, 541, DateTimeKind.Local).AddTicks(5431), false, new DateTime(2021, 6, 4, 0, 37, 44, 541, DateTimeKind.Local).AddTicks(5452), "Studing" },
                    { -3, new DateTime(2021, 6, 4, 0, 37, 44, 541, DateTimeKind.Local).AddTicks(5477), false, new DateTime(2021, 6, 4, 0, 37, 44, 541, DateTimeKind.Local).AddTicks(5481), "Working" },
                    { -4, new DateTime(2021, 6, 4, 0, 37, 44, 541, DateTimeKind.Local).AddTicks(5485), false, new DateTime(2021, 6, 4, 0, 37, 44, 541, DateTimeKind.Local).AddTicks(5489), "On vacation" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "DateCreated", "IsDeleted", "LastModified", "TagName" },
                values: new object[,]
                {
                    { -12, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3270), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3273), "React" },
                    { -11, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3264), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3267), "Angular" },
                    { -10, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3258), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3260), "Teamlead" },
                    { -9, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3250), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3253), "Unity" },
                    { -8, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3244), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3246), "Lisp" },
                    { -7, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3237), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3240), "Manager" },
                    { -4, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3215), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3218), "DevOps" },
                    { -5, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3221), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3224), "Java" },
                    { -13, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3277), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3279), "Vue" },
                    { -3, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3208), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3211), "C#" },
                    { -2, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3182), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3195), "Phyton" },
                    { -1, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(2556), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(2577), "JavaScript" },
                    { -6, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3231), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3234), "C/C++" },
                    { -14, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3283), false, new DateTime(2021, 6, 4, 0, 37, 44, 542, DateTimeKind.Local).AddTicks(3286), "TypeScript" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "City", "ConcurrencyStamp", "Country", "Email", "EmailConfirmed", "FirstName", "FullName", "ImageLink", "Job", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "School", "SecurityStamp", "Speciality", "StatusId", "TwoFactorEnabled", "University", "UserName" },
                values: new object[] { -1, 0, "Kiev", "f36f9b16-7087-42c5-87f9-3540d01d797c", "Ukraine", "admin@gmail.com", false, "Admin", "Admin Admin", "https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png", "Accounting system portal", "Admin", true, null, "ADMIN@GMAIL.COM", "ADMIN@GMAIL.COM", "AQAAAAEAACcQAAAAEGFiPeNm+APoDfdmitkds8n489XLLmnZkK31KW2ppNLjxg+hK3TLljaTp6yLCG/c0Q==", null, false, "Some school", "e8fe0256-e9b3-48b0-85e1-dc8560893baf", "Computer science", -1, false, "Some university", "admin@gmail.com" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { -1, 1 });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_StatusId",
                table: "AspNetUsers",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserReceiverId",
                table: "Comments",
                column: "UserReceiverId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserSenderId",
                table: "Comments",
                column: "UserSenderId");

            migrationBuilder.CreateIndex(
                name: "IX_CommentTags_TagId",
                table: "CommentTags",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_TagDesctriptions_TagId",
                table: "TagDesctriptions",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_TagDesctriptions_UserId",
                table: "TagDesctriptions",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CommentTags");

            migrationBuilder.DropTable(
                name: "TagDesctriptions");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Statuses");
        }
    }
}
