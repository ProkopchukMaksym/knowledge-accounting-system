export interface UserTokenModel {
  id: number;
  fullName: string;
  role: string;
  token: string;
}
